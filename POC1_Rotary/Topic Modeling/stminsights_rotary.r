## Topic modeling exploration using Shiny app' "stminsights"
# Introduction: https://cran.r-project.org/web/packages/stminsights/vignettes/intro.html 
# GitHub: devtools::install_github('cschwem2er/stminsights')

# load package 
library(stm)
library(quanteda)
library(stminsights)
library(tidyverse)
library(ggraph)

# load and prepare data
library(readr)
rotarytm <- read_csv("Data/rotarytm.csv") 

# pre-processing
meta <- rotarytm %>% transmute(DocID, title, publisher, category, 
                               date, year, decade, period, 
                               nchar, ntoken, nsentence)
corpus <- stm::textProcessor(rotarytm$fulltext, 
                             metadata = meta, 
                             stem = FALSE, 
                             wordLengths = c(4, Inf), 
                             customstopwords = stopword_rotary)
out <- stm::prepDocuments(corpus$documents, 
                          corpus$vocab, 
                          corpus$meta, 
                          lower.thresh = 10) 

# Our corpus has 932 documents, 4106 terms and 177557 tokens.  

# Fit models and effect estimates

# Build multiple models ranging from 5 to 50 topics 

set.seed(1234)
manymodels <- manyTopics(out$documents,
                      out$vocab,
                      K=c(5,10,15,20,25,30,35,40,45,50), 
                      prevalence =~ publisher + category + year, 
                      data=out$meta, 
                      verbose = FALSE)

# we select four models with 5, 20 and 50 topics 
mod.5 <- manymodels$out[[1]] 
mod.10 <- manymodels$out[[2]] 
mod.20 <- manymodels$out[[4]] 
mod.50 <- manymodels$out[[10]] 

# alternative (faster) solution 
mod.5 <- stm::stm(out$documents, 
                   out$vocab, K=5, 
                   prevalence =~ publisher + category + year, 
                   data=out$meta, verbose = FALSE)

mod.10 <- stm::stm(out$documents, 
                   out$vocab, K=10, 
                   prevalence =~ publisher + category + year, 
                   data=out$meta, verbose = FALSE)

mod.20 <- stm::stm(out$documents, 
                   out$vocab, K=20, 
                   prevalence =~ publisher + category + year, 
                   data=out$meta, verbose = FALSE)

mod.50 <- stm::stm(out$documents, 
                   out$vocab, K=50, 
                   prevalence =~ publisher + category + year, 
                   data=out$meta, verbose = FALSE)

# Estimate effect for our three models 

# 5-topic model

# time effect 
preptime5 <- stm::estimateEffect(1:5 ~ year, mod.5, meta=out$meta)
# source effect 
prepsource5 <- estimateEffect(c(1:5) ~ publisher, mod.5, metadata=out$meta, 
                               uncertainty="None")
# genre effect
prepgenre5 <- estimateEffect(c(1:5) ~ category, mod.5, metadata=out$meta, 
                              uncertainty="None")

# 5-topic model

# time effect 
preptime10 <- stm::estimateEffect(1:10 ~ year, mod.10, meta=out$meta)
# source effect 
prepsource10 <- estimateEffect(c(1:10) ~ publisher, mod.10, metadata=out$meta, 
                              uncertainty="None")
# genre effect
prepgenre10 <- estimateEffect(c(1:10) ~ category, mod.10, metadata=out$meta, 
                             uncertainty="None")

# 20-topic model

# time effect 
preptime20 <- stm::estimateEffect(1:20 ~ year, mod.20, meta=out$meta)
# source effect 
prepsource20 <- estimateEffect(c(1:20) ~ publisher, mod.20, metadata=out$meta, 
                             uncertainty="None")
# genre effect
prepgenre20 <- estimateEffect(c(1:20) ~ category, mod.20, metadata=out$meta, 
                            uncertainty="None")

# 50-topic model

# time effect 
preptime50 <- stm::estimateEffect(1:50 ~ year, mod.50, meta=out$meta)
# source effect 
prepsource50 <- estimateEffect(c(1:50) ~ publisher, mod.50, metadata=out$meta, 
                             uncertainty="None")
# genre effect
prepgenre50 <- estimateEffect(c(1:50) ~ category, mod.50, metadata=out$meta, 
                            uncertainty="None")

# save objects in .RData file
save.image('rotarytopics.RData')

# load and run stminsights

library(stminsights)
run_stminsights()


# Utility functions 

# get_diag(): create a dataframe including statistical diagnostics for several models
# get_network(): create a tidygraph for a correlation network of stm topics
# get_effects(): create a dataframe including prevalence effects for one stm model

# get diagnostics

diag <- get_diag(models = list(
  rotary_5 = mod.5,
  rotary_10 = mod.10, 
  rotary_20 = mod.20,
  rotary_50 = mod.50),
  outobj = out)

# plot diagnostics

diag %>%
  ggplot(aes(x = coherence, y = exclusivity, color = statistic))  +
  geom_text(aes(label = name), nudge_x = 5) + geom_point() +
  labs(x = 'Semantic Coherence', y = 'Exclusivity') + theme_light()


# get_network()

# extract network
stm_corrs10 <- get_network(model = mod.10,
                         method = 'simple',
                         labels = paste('Topic', 1:10),
                         cutoff = 0.001,
                         cutiso = TRUE)

stm_corrs20 <- get_network(model = mod.20,
                           method = 'simple',
                           labels = paste('Topic', 1:20),
                           cutoff = 0.001,
                           cutiso = TRUE)

stm_corrs50 <- get_network(model = mod.50,
                           method = 'simple',
                           labels = paste('Topic', 1:50),
                           cutoff = 0.001,
                           cutiso = TRUE)

# plot network
ggraph(stm_corrs10, layout = 'fr') +
  geom_edge_link(
    aes(edge_width = weight),
    label_colour = '#fc8d62',
    edge_colour = '#377eb8') +
  geom_node_point(size = 4, colour = 'black')  +
  geom_node_label(
    aes(label = name, size = props),
    colour = 'black',  repel = TRUE, alpha = 0.85) +
  scale_size(range = c(2, 10), labels = scales::percent) +
  labs(size = 'Topic Proportion',  edge_width = 'Topic Correlation') +
  scale_edge_width(range = c(1, 3)) +
  theme_graph()

ggraph(stm_corrs20, layout = 'fr') +
  geom_edge_link(
    aes(edge_width = weight),
    label_colour = '#fc8d62',
    edge_colour = '#377eb8') +
  geom_node_point(size = 4, colour = 'black')  +
  geom_node_label(
    aes(label = name, size = props),
    colour = 'black',  repel = TRUE, alpha = 0.85) +
  scale_size(range = c(2, 8), labels = scales::percent) +
  labs(size = 'Topic Proportion',  edge_width = 'Topic Correlation') +
  scale_edge_width(range = c(1, 4)) +
  theme_graph()

ggraph(stm_corrs50, layout = 'fr') +
  geom_edge_link(
    aes(edge_width = weight),
    label_colour = '#fc8d62',
    edge_colour = '#377eb8') +
  geom_node_point(size = 4, colour = 'black')  +
  geom_node_label(
    aes(label = name, size = props),
    colour = 'black',  repel = TRUE, alpha = 0.85) +
  scale_size(range = c(1, 4), labels = scales::percent) +
  labs(size = 'Topic Proportion',  edge_width = 'Topic Correlation') +
  scale_edge_width(range = c(1, 4)) +
  theme_graph() 
  

# get effects 

# time effect (year)

effect_time_20 <- get_effects(estimates = preptime20,
                       variable = 'year',
                       type = 'continuous')


# plot effects

effect_time_20 %>% filter(topic == 3) %>%
  ggplot(aes(x = value, y = proportion)) +
  geom_line() + 
  theme_light() + 
  labs(x = 'Year', y = 'Topic Proportion', 
       title = "Time effect on topic 3", 
       caption = "based on 20-topic model of the Rotary corpus")


effects %>% filter(topic == 3) %>%
  ggplot(aes(x = value, y = proportion)) +
  geom_errorbar(aes(ymin = lower, ymax = upper), width = 0.1, size = 1) +
  geom_point(size = 3) +
  coord_flip() + theme_light() + labs(x = 'Treatment', y = 'Topic Proportion')


# source effect (periodical/publisher) 

effect_source_20 <- get_effects(estimates = prepsource20,
                         variable = 'publisher',
                         type = 'pointestimate')

effect_source_20 %>% filter(topic == 3) %>%
  ggplot(aes(x = value, y = proportion)) +
  geom_errorbar(aes(ymin = lower, ymax = upper), width = 0.1, size = 1) +
  geom_point(size = 3) +
  coord_flip() + theme_light() + 
  labs(x = 'Periodical', y = 'Topic Proportion', 
       title = "Genre effect on topic 3", 
       caption = "based on 20-topic model of the Rotary corpus")

# genre effect (category) 

effect_genre_20 <- get_effects(estimates = prepgenre20,
                         variable = 'category',
                         type = 'pointestimate')

effect_genre_20 %>% filter(topic == 3) %>%
  ggplot(aes(x = value, y = proportion)) +
  geom_errorbar(aes(ymin = lower, ymax = upper), width = 0.1, size = 1) +
  geom_point(size = 3) +
  coord_flip() + theme_light() + 
  labs(x = 'Genre of article', y = 'Topic Proportion', 
       title = "Genre effect on topic 3", 
       caption = "based on 20-topic model of the Rotary corpus")



