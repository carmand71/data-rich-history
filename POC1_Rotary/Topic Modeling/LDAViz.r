# Create a LDV visualization of 20-topic model of the Rotary Club dataset 

# load packages 
library(tidyverse)
library(stm)
library(htmlwidgets)

# load data

library(readr)
rotarytm <- read_csv("Data/rotarytm.csv", col_types = cols(...1 = col_skip())) 
rotarytm <- rotarytm %>% mutate(Year = as.integer(stringr::str_sub(Date,0,4))) # create a variable for years 
rotarytm$decade <- paste0(substr(rotarytm$Date, 0, 3), "0") # create a variable for decades
rotarytm <- rotarytm %>% select(DocID, Date, Year, decade, Title, Source, Section, Text)


# Pre-processing
meta <- rotarytm %>% transmute(DocID, Title, Source, Section, Year, decade)
corpus <- stm::textProcessor(rotarytm$Text, 
                             metadata = meta, 
                             stem = FALSE, 
                             wordLengths = c(4, Inf), 
                             customstopwords = c("rotary", "club", "shanghai", "rotarian", "china", "chinese", "will", "one", "two"))

# Filtering
out <- stm::prepDocuments(corpus$documents, corpus$vocab, corpus$meta, lower.thresh = 10) 

# Build the model with 20 topics
mod.20 <- stm::stm(out$documents, out$vocab, K=20, prevalence =~ Source + Section + Year, data=out$meta)

# Visualize 

p <- stm::toLDAvis(mod.20, doc=out$documents)

# save the widget
saveWidget(p, file=paste0( getwd(), "/Rotary20topic.html"))

