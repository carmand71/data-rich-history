# rebuild the Rotary corpus using histtext

# Compared to the previous version, compared to its predecessor - enpchina - the new package histtext includes advanced search and filtering function, additional collections (South China Morning Post) and metadata (e.g. category/genre of article). 
# More details on histext here (updated manual) (https://bookdown.enpchina.eu/rpackage/1.0.0.html)

# load packages
library(histtext)
library(enpchina)
library(tidyverse)
library(quanteda)
library(plotly)
library(tidytext)
library(stm)
library(stminsights)

# build a corpus related to the Rotary Club of Shanghai (we use the same terms as those used for creating the corpus with the enp-china package) 

rotary_search <- histtext::search_documents_ex('"Shanghai Rotary", "Rotary Club of Shanghai"', "proquest")

# In the next step, we use the function "get_search_fields_content()" to specify some particular features we want to retrieve more specifically: 
  # title: the title of the article (title)
  # date : the date of publication 
  # publisher: the name of the periodical in which the article appeared 
  # category: the genre of the article
  # fulltext : the plain text of the article
# Note: only the "category" field needs to be specified. all other categories come y default with a simple search

rotary_doc <- histtext::get_search_fields_content(rotary_search, "proquest",
                                            search_fields = c("title", "date", "publisher", "category", "fulltext"),
                                            verbose = FALSE) 
# the corpus contains 1027 documents (including Hongkong periodicals)

# distribution across periodicals (publishers)
rotary_doc %>% 
  group_by(publisher) %>% 
  count(sort = TRUE)

# focus on mainland periodicals 
rotary_mainland <- rotary_doc %>% filter(!publisher %in% c("South China Morning Post Publishers Limited", "South China Morning Post Ltd."))
# the mainland corpus contains 936 documents (not including Hongkong periodicals)

# create variable for years   
rotary_mainland <- rotary_mainland %>%
  mutate(year = stringr::str_sub(date,0,4)) %>% 
  mutate(year = as.numeric(year))

# create variable for decade  
rotary_mainland$decade <- paste0(substr(rotary_mainland$date, 0, 3), "0")

# create factor variable for period 
# periodization based on expert/contextual knowledge: three periods based on our knowledge of the history of the Rotary and of the political context in pre-1949 China

rotary_mainland$period <- cut(rotary_mainland$year, breaks = c(1919, 1929, 1937, 1948), 
                               label = c("1919-1929", "1930-1937", "1938-1948"), 
                               include.lowest = TRUE) 

# factorize category of article ? 

rotary_mainland %>% 
  group_by(category) %>% 
  count(sort = TRUE)

# discard irrelevant categories ?? 
rotary_mainland_filtered <- rotary_mainland %>% 
  filter(!category == "Table of Contents; Front Matter")

histtext::view_document("1369907721", "proquest", query = '"rotary"')
histtext::view_document("1369871374", "proquest", query = '"rotary"')
histtext::view_document("1420025525", "proquest", query = '"rotary"')
histtext::view_document("1420037929", "proquest", query = '"rotary"')


# the filtered corpus contains 932 documents

# distribution over time 

rotary_mainland_filtered %>%  
  group_by(year) %>% count() %>% 
  filter (year>=1919 & year<=1950) %>% 
  ggplot(aes(year,n)) + geom_col(alpha = 0.8) + 
  labs(title = "The Rotary Club of Shanghai in the English-language press",
       caption = "Based on data extracted from ProQuest 'Chinese Historical Newspapers' collection",
       subtitle = "Number of articles mentioning the club",
       x = "Year", 
       y = "Number of articles")


# distribution over time 

# barplot
rotary_mainland_filtered %>% 
  group_by(year) %>% 
  count(publisher) %>%
  ggplot(aes(x=year, y=n, fill=publisher)) + 
  geom_col(alpha = 0.8) + 
  theme(legend.position="bottom", 
        legend.text = element_text(size=8)) + 
  labs(title = "The Rotary Club of Shanghai in the English-language press (1919-1948)", 
       subtitle = "Distribution across periodicals over time", 
       caption = "Based on data extracted from ProQuest 'Chinese Historical Newspapers' collection",
       x = "Year", 
       y = "Number of articles", 
       fill = "Periodical", 
       size = 2)

# boxplot
rotary_mainland_filtered %>%
  ggplot(aes(reorder(publisher, year), year, color = publisher)) +
  geom_boxplot(alpha = 0.8, show.legend = FALSE) +
  coord_flip()+
  labs(x = "Periodical", y = "Year")+  
  labs(title = "The Rotary Club of Shanghai in the English press (1919-1948)", 
       subtitle = "Distribution across periodicals over time",
       caption = "Based on data extracted from ProQuest 'Chinese Historical Newspapers' collection")  

# length of articles 
# this can help to detect anomalies (issues in segmentation that may impact downstream analysis, particularly the outputs of topic modeling output)
rotary_mainland_filtered <- rotary_mainland_filtered %>% 
  mutate(nsentence = nsentence(fulltext))  %>% 
  mutate(ntoken = ntoken(fulltext)) %>% 
  mutate(nchar = nchar(fulltext)) 

# plot length of articles

rotary_mainland_filtered %>%
  ggplot(aes(x=nsentence)) +
  geom_histogram(show.legend = FALSE, alpha = 0.8) +
  labs(title = "Length of articles in the Rotary corpus", 
       x = "Number of sentences per article",
       y = "Number of articles", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")

rotary_mainland_filtered %>%
  ggplot(aes(x=ntoken)) +
  geom_histogram(show.legend = FALSE, alpha = 0.8) +
  labs(title = "Length of articles in the Rotary corpus", 
       x = "Number of tokens per article",
       y = "Number of articles", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")

rotary_mainland_filtered %>%
  ggplot(aes(x=nchar)) +
  geom_histogram(show.legend = FALSE, alpha = 0.8) +
  labs(title = "Length of articles in the Rotary corpus", 
       x = "Number of characters per article",
       y = "Number of articles", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")

# close reading of anomalous articles (>30,000 characters) = often but not always pb of segmentation
histtext::view_document("1371324143", "proquest", query = '"Rotary"') # >60,000
histtext::view_document("1369872303", "proquest", query = '"Rotary"') # >40,000
histtext::view_document("1369970997", "proquest", query = '"Rotary"') # >30,000 # long article, no segmentation problems
histtext::view_document("1371423966", "proquest", query = '"Rotary"') # >30,000 # long article, no segmentation problems
# Articles with more than 40,000 systematically refer to segmentation problems. 
# Some articles below 40,000 may or may not point to segmentation problems

histtext::view_document("1324697239", "proquest", query = '"Rotary"') # >20,000 : list of donators (Red Cross Flood-Famine fund) 
histtext::view_document("1321244768", "proquest", query = '"Rotary"') # >20,000 : two-page article
histtext::view_document("1324685125", "proquest", query = '"Rotary"') # >20,000 : long article
histtext::view_document("1319876038", "proquest", query = '"Rotary"')  # >20,000 : long article

histtext::view_document("1319904277", "proquest", query = '"Rotary", "rotary"') # > 15,000 : segmentation problem (CWR - Men and Events)
histtext::view_document("1371510195", "proquest", query = '"Rotary", "rotary"') # > 15,000 : segmentation problem (CWR - People in the News)
histtext::view_document("1324848229", "proquest", query = '"Rotary", "rotary"') # > 15,000 : segmentation problem (China Press - Brevities : Local and General)
histtext::view_document("1371892336", "proquest", query = '"Rotary", "rotary"') # China Press - News Brevities
histtext::view_document("1416600595", "proquest", query = '"Rotary", "rotary"') # China Press - Display Ad 
histtext::view_document("1371428349", "proquest", query = '"Rotary", "rotary"') # North China Herald - Personal Notes
histtext::view_document("1324802562", "proquest", query = '"Rotary", "rotary"') # Shanghai Times - Local and General

# On the oppposite, very short article (just one sentence) usually refer to photo captions on illustrated pages or point to missing punctuation or incomplete articles

histtext::view_document("1425765615", "proquest", query = '"Rotary", "rotary"') # China Press - Image/photograph (photo caption)
histtext::view_document("1420031469", "proquest", query = '"Rotary", "rotary"') # North China Herald - Feature; Article (missing punctuation)
histtext::view_document("1371635451", "proquest", query = '"Rotary", "rotary"') # China Press - Front Page / Cover Story (incomplete article)
histtext::view_document("1371335334", "proquest", query = '"Rotary", "rotary"') # Letter to the Editor; Correspondence (missing punctuation)

histtext::view_document("1420035315", "proquest", query = '"Rotary", "rotary"') # North China Herald - Feature; Article (incomplete article ?)
histtext::view_document("1418985785", "proquest", query = '"Rotary", "rotary"') # North China Herald - Front Page/Cover Story - Article X - No Title (incomplete article ?)
histtext::view_document("1426585723", "proquest", query = '"Rotary", "rotary"') # North China Herald - Front Page/Cover Story - Article X - No Title (incomplete article ?)



# problems with the ready-to-use classification/categorization: short articles (Men and Events, People in the News, Brevities) are classified under the "Feature - Article" category, along with "genuine", coherent articles

# length over time

p1 <- rotary_mainland_filtered %>%
  group_by(year, nchar) %>% 
  add_tally() %>% 
  tally(mean(nchar)) %>%
  group_by(year) %>% 
  tally(mean(n)) %>%
  arrange(desc(n)) %>%  
  ggplot() + 
  geom_col(aes(x = year, y = n), alpha = 0.8) + 
  labs(title = "Length of articles in the Rotary corpus", 
       subtitle = "Length over time",
       x = "Year",
       y = "Average number of characters",
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")


fig1 <- ggplotly(p1) %>% hide_legend()

fig1

# length by periodicals


rotary_mainland_filtered %>%
  ggplot(aes(reorder(publisher, nchar), nchar, color = publisher)) +
  geom_boxplot(alpha = 0.8, show.legend = FALSE) +
  coord_flip()+
  labs(x = "Periodical", y = "Number of characters")+  
  labs(title = "Length of articles in the Rotary corpus", 
       subtitle = "Length and periodicals",
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")  


rotary_mainland_filtered %>% 
  ggplot(aes(x=nchar, fill = publisher)) +
  geom_histogram(show.legend = FALSE) +
  facet_wrap(~publisher, nrow = 2, scale = "free_y")+
  labs(title = "Length of articles in the returned students press corpus", 
       subtitle = "Length across periodicals",
       x = "Number of characters",
       y = "Number of articles", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")

# length by genre


rotary_mainland_filtered %>%
  ggplot(aes(reorder(category, nchar), nchar, color = category)) +
  geom_boxplot(alpha = 0.8, show.legend = FALSE) +
  coord_flip()+
  labs(x = "Genre", y = "Number of characters")+  
  labs(title = "Length of articles in the Rotary corpus", 
       subtitle = "Length and genre",
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"") 

# length, genre and publisher

# Best
rotary_mainland_filtered %>%
  ggplot(aes(reorder(publisher, nchar), nchar, color = category)) +
  geom_boxplot(alpha = 0.8, show.legend = TRUE) + 
  theme(legend.position="bottom", 
        legend.text = element_text(size=8)) +
  labs(x = "Periodical", y = "Number of characters")+  
  labs(title = "Length of articles in the Rotary corpus", 
       subtitle = "Length, genre and periodical",
       color = "Genre", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")


rotary_mainland_filtered %>%
  ggplot(aes(reorder(publisher, nchar), nchar, color = category)) +
  geom_boxplot(alpha = 0.8, show.legend = TRUE) + 
  coord_flip()+
  theme(legend.position="bottom", 
        legend.text = element_text(size=7)) +
  labs(x = "Periodical", y = "Number of characters")+  
  labs(title = "Length of articles in the Rotary corpus", 
       subtitle = "Length, genre and periodical",
       color = "Genre", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"")



rotary_mainland_filtered %>%
  ggplot(aes(reorder(category, nchar), nchar, color = publisher)) +
  geom_boxplot(alpha = 0.8, show.legend = TRUE) + 
  theme(legend.position="bottom", 
        legend.text = element_text(size=8)) +
  labs(x = "Genre of article", y = "Number of characters")+  
  labs(title = "Length of articles in the Rotary corpus", 
       subtitle = "Length, genre and periodical",
       color = "Periodical", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"") 


rotary_mainland_filtered %>%
  ggplot(aes(reorder(category, nchar), nchar, color = publisher)) +
  geom_boxplot(alpha = 0.8, show.legend = TRUE) + 
  coord_flip()+
  theme(legend.position="bottom", 
        legend.text = element_text(size=8)) +
  labs(x = "Genre of article", y = "Number of characters")+  
  labs(title = "Length of articles in the Rotary corpus", 
       subtitle = "Length, genre and periodical",
       color = "Periodical", 
       caption = "Based on ProQuest \"Chinese Newspapers Collection (CNC)\"") 


# Logistic regression to analyze correlation between category of article/publisher and length of article? 

### Prepare text data for topic modeling

# identify non words and other words to discard

tidy_rotary <- rotary_mainland_filtered %>%
  mutate(line = row_number()) %>%
  unnest_tokens(word, fulltext) %>%
  anti_join(stop_words) %>% 
  filter(!str_detect(word, '[0-9]{1,}')) %>% 
  filter(nchar(word) > 3) 

tidy_rotary_count <- tidy_rotary %>%
  count(word, sort = TRUE)

stopword_rotary <- c("american", "china", "chinese", "china's", "club", "shanghai", "tion", "tions", "tlie", "tlio", "chin", "ameri", "university")

# prepare corpus 

rotarytm <- rotary_mainland_filtered
write.csv(rotarytm, "rotarytm.csv") # save as csv file

meta <- rotarytm %>% transmute(DocID, title, publisher, category, date, year, decade, period, nchar, ntoken, nsentence)
corpus <- stm::textProcessor(rotarytm$fulltext, 
                             metadata = meta, 
                             stem = FALSE, 
                             wordLengths = c(4, Inf), 
                             customstopwords = stopword_rotary)

# pre-processing
meta <- rotarytm %>% transmute(DocID, title, publisher, category, 
                               date, year, decade, period, 
                               nchar, ntoken, nsentence)
corpus <- stm::textProcessor(rotarytm$fulltext, 
                             metadata = meta, 
                             stem = FALSE, 
                             wordLengths = c(4, Inf), 
                             customstopwords = stopword_rotary)
# Our corpus contains 932 documents, and 42651 word dictionary.  
out <- stm::prepDocuments(corpus$documents, 
                          corpus$vocab, 
                          corpus$meta, 
                          lower.thresh = 10) 


# After removing words with less than 4 characters and words that appear less than 10 times in the corpus, 
# The final corpus corpus contains 932 documents, 4106 terms and 177557 tokens.


#  Alternative: join rotarytm with the new corpus to obtain the same list of documents but with the additional metadata (category, length of article, periodization)

rotarytm <- inner_join(rotarytm, rotary_mainland_filtered, by = "DocID")






