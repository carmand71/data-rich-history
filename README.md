# Data-rich history

This project calls for a new “data-rich” methodology in history. The “Methodology” section defines the concept and its major components through various diagrams. “POC” (proof of concept) sections contain the materials (input datasets, code, outputs) required to conduct the various pilot projects that aim to demonstrate the validity of this approach. This note also includes a glossary of key terms employed throughout the project, and a list of references that have helped me to conceptualize this approach.  

This is a work in progress. More materials will be added as the research progresses. Currently, there are two main proofs of concepts: the Rotary Club of China (POC1) and American University Men of China (POC2). The two directories contain materials related to corpus building and topic modeling. In addition, POC1 includes the scripts used for creating the flow chart showing the Rotarians' trajectories. 

***

**Glossary**

**Digital archive**: a multilayered structure consisting of a mixed set of documents* collated from a wide range of sources for research purposes. As the largest documentary unit, it is composed of various collections* with varying level of digitization, each with its own (material) history. A digital archive constitutes a stable system of representation (Bode, 2017):
-	of which we can track, reconstruct and document the process of creation and transformation; 
-	from which historians can create their own corpora* and micro-editions* to meet their research needs and further enable citation and replicability. 

**Digital collection**: a subset of the digital archive. It holds at the intermediate level between the digital archive and the digital document. A digital collection is a coherent set of documents created by, and collated from an external source (public institution, private person, enterprise) (e.g. ProQuest Historical Newspapers, IMH Who’s Who collection, students’ journals from HathiTrust and university libraries or archives) – from which historians can create their own corpus or corpora to accommodate their specific research needs. 

**Digital corpus**: at a lower level, a digital corpus is a set of documents created by a historian for the purpose of a specific research. 
-	It may draw documents from one or several different source collections. 
-	It can be treated as a safely bounded scientific dataset on which various computational tasks can be applied to perform a wide range of analyses. 
-	As the whole chain of operations is meticulously documented, any other researcher can reconstruct the corpus and fully replicate the workflow. 

**Micro-editions** are citable subsets of the corpus that can both be related to the reference corpus and archive, and used in historical narratives to support an argument. The term was coined by Cordell and Smith (2019) as a workable alternative to the ideal model of stable “scholarly edition”  proposed by Bode, 2017.  

**Document**: At a lower level, each corpus can be subdivided into documents. Documents in turn break down into sentences, words, tokens, and characters, as smaller pieces of information that can be turned into data and then subjected to multiple manipulations. Data finally represents the most elementary unit in the digital archive, comparable to atoms in the universe.

***

**References** 

Bode, Katherine. “The Equivalence of ‘Close’ and ‘Distant’ Reading; or, Toward a New Object for Data-Rich Literary History.” Modern Language Quarterly 78, no. 1 (March 1, 2017): 77–106. https://doi.org/10.1215/00267929-3699787.

Clavert, Frédéric, and Andreas Fickers. “On Pyramids, Prisms, and Scalable Reading Journal of Digital History.” Journal of Digital History 1, no. 1 (October 28, 2021). https://journalofdigitalhistory.org/en/article/jXupS3QAeNgb. 

Cordell, Ryan, and David A. Smith. “Textual Criticism as Language Modeling by David A. Smith, Ryan Cordell.” In Going the Rounds: Virality in Nineteenth-Century American Newspapers. Minneapolis, MN: University of Minnesota Press, 2019. https://manifold.umn.edu/projects/going-the-rounds.

Jo, Eun Seo. “Foreign Relations of the United States Series, 1860-1980: A Study in New Archival History.” Thesis Ph.D. in History, Stanford University, 2020.
Schöch, Christof. “Big? Smart? Clean? Messy? Data in the Humanities.” Journal of Digital Humanities 2, no. 3 (December 2013): 2–13.
